<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\postContent
class postContentCotroller extends Controller
{
   public function index()
    {
        return postContent::all();
    }

    public function show(postContent $post)
    {
        return $post;
    }

    public function store(Request $request)
    {
        $post = postContent::create($request->all());

        return response()->json($post, 201);
    }

    public function update(Request $request, postContent $post)
    {
        $post->update($request->all());

        return response()->json($post, 200);
    }

    public function delete(postContent $post)
    {
        $post->delete();

        return response()->json(null, 204);
    }
}
