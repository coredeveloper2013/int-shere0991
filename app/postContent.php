<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postContent extends Model
{
    protected $fillable = ['title', 'body'];
}
